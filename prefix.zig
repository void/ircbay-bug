const std = @import("std");
const net = std.net;

const mem = std.mem;

const testing = std.testing;
const expect = testing.expect;
const expectEqualStrings = testing.expectEqualStrings;

const Prefix = @This();

name: []const u8,
user: ?[]const u8,
host: ?[]const u8,

pub fn parse(string: []const u8) Prefix {
    const user_start = mem.indexOf(u8, string, "!");
    const host_start = mem.indexOf(u8, string, "@");

    return .{
        .name = if (user_start) |start|
            string[0..start]
        else if (host_start) |start|
            string[0..start]
        else
            string,

        .user = if (user_start) |start|
            string[start + 1 .. host_start orelse string.len]
        else
            null,

        .host = if (host_start) |start|
            string[start + 1 ..]
        else
            null,
    };
}

test "Parse prefix with just name" {
    const prefix = Prefix.parse("hello");
    expectEqualStrings("hello", prefix.name);
    expect(prefix.user == null);
    expect(prefix.host == null);
}
test "Parse prefix with user and host" {
    const prefix = Prefix.parse("hello!hello@hello.net");
    expectEqualStrings("hello", prefix.name);
    expect(prefix.user != null);
    expectEqualStrings("hello", prefix.user.?);
    expect(prefix.host != null);
    expectEqualStrings("hello.net", prefix.host.?);
}
test "Parse prefix and host" {
    const prefix = Prefix.parse("hello@hello.net");
    expectEqualStrings("hello", prefix.name);
    expect(prefix.user == null);
    expect(prefix.host != null);
    expectEqualStrings("hello.net", prefix.host.?);
}
test "Parse invalid prefix with empty user" {
    const prefix = Prefix.parse("hello!");
    expectEqualStrings("hello", prefix.name);
    expect(prefix.user != null);
    expectEqualStrings("", prefix.user.?);
    expect(prefix.host == null);
}
test "Parse invalid prefix with empty host" {
    const prefix = Prefix.parse("hello@");
    expectEqualStrings("hello", prefix.name);
    expect(prefix.user == null);
    expect(prefix.host != null);
    expectEqualStrings("", prefix.host.?);
}
test "Parse invalid prefix that's empty" {
    const prefix = Prefix.parse("");
    expectEqualStrings("", prefix.name);
    expect(prefix.user == null);
    expect(prefix.host == null);
}
