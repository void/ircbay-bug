pub const io_mode = .evented;

const std = @import("std");
const net = std.net;

const mem = std.mem;
const Allocator = mem.Allocator;

const Prefix = @import("prefix.zig");

test "All tests" {
    _ = Prefix;
}

const Config = struct { host: []u8, port: u16, user: []u8 };

fn getArgs(allocator: *Allocator) !Config {
    var config = Config{
        .host = undefined,
        .port = undefined,
        .user = undefined,
    };

    var args = std.process.args();
    defer args.deinit();

    if (!args.skip()) return error.NotEnoughArgs;

    if (args.next(allocator)) |host| {
        config.host = try host;
    } else return error.NotEnoughArgs;
    if (args.next(allocator)) |port| {
        config.port = try std.fmt.parseInt(u16, try port, 10);
    } else return error.NotEnoughArgs;
    if (args.next(allocator)) |user| {
        config.user = try user;
    } else return error.NotEnoughArgs;

    return config;
}

const RawMessage = struct {
    prefix: ?Prefix,
    command: []const u8,
    parameters: []const u8,

    pub fn parse(message: []const u8) !RawMessage {
        var parts = std.mem.split(message, " ");

        var prefix: ?Prefix = null;
        var command: ?[]const u8 = null;

        while (command == null) {
            if (parts.next()) |part| {
                if (part[0] == ':') {
                    prefix = Prefix.parse(part[1..]);
                } else {
                    command = part;
                }
            } else return error.UnableToParse;
        }

        return RawMessage{
            .prefix = prefix,
            .command = command.?,
            .parameters = parts.rest(),
        };
    }
};

pub fn handleMessage(message: []const u8) !void {
    std.debug.print("{s}\n", .{message});
    const raw = RawMessage.parse(message);
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = &gpa.allocator;

    const config = try getArgs(allocator);
    std.debug.print("Connecting to {s}:{}\n", .{ config.host, config.port });

    const conn = try net.tcpConnectToHost(allocator, config.host, config.port);
    const reader = std.io.bufferedReader(conn.reader()).reader();
    const writer = conn.writer();

    try writer.print("NICK {s}\r\n", .{config.user});
    try writer.print("USER {s} ircbay ircbay :Real {s}\r\n", .{ config.user, config.user });

    const loop_handle = async commandLoop(writer);

    while (try untilChar(512, '\n', reader)) |message| {
        try handleMessage(message[0 .. message.len - 1]); // remove \r
    }

    std.debug.print("Quitting...\n", .{});
}

fn untilChar(comptime buffer_size: u64, char: u8, reader: anytype) !?[]u8 {
    var buffer: [buffer_size]u8 = undefined;
    var i: u64 = 0;
    while (i < buffer.len - 1) {
        buffer[i] = reader.readByte() catch |err| switch (err) {
            error.EndOfStream => return null,
            else => return err,
        };

        if (buffer[i] == char) {
            return buffer[0..i];
        }

        i += 1;
    }
    return error.LineTooLong;
}

pub fn commandLoop(writer: net.Stream.Writer) !void {
    const reader = std.io.getStdIn().reader();
    while (try untilChar(4096, '\n', reader)) |line| {
        //std.debug.print("Line: {s}\n", .{line});
        var parts = std.mem.split(line, " ");
        if (parts.next()) |command| {
            if (mem.eql(u8, command, "/join")) {
                const channels = parts.rest();
                std.debug.print("/join: {s}\n", .{channels});
                try writer.print("JOIN {s}\r\n", .{channels});
            } else {
                std.debug.print("message: {s}\n", .{line});
                // XXX
                try writer.print("{s}\r\n", .{line});
            }
        }
    }
}
